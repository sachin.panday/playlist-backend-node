import { Arg, FieldResolver, Query, Resolver, Root } from "type-graphql";
import { LibraryData } from "../data";
import Library from "../schemas/Library";
import { LibraryService } from "../service/library.service"
import "reflect-metadata";
import {container} from "tsyringe";

@Resolver(of => Library)
export default class {
  public libraryService: LibraryService = container.resolve(LibraryService);

  /**
   * get Complete list of library
   */
  @Query(returns => [Library], { nullable: true })
  fetchLibrary(): LibraryData[] | undefined {
    const response = this.libraryService.getLibrarys();
    return response;
  }

  /**
   * get library using library Id
   * @param id
   */
  @Query(returns => Library, { nullable: true })
  getLibrary(@Arg("id") id: number): LibraryData | undefined {
    if(!id){
      throw new SyntaxError("Library id required");
    }
    const response = this.libraryService.getLibrarys();
    return response.find(library => library.id === id);
  }

  /**
   * get library using searchString which apply on album, title and artist
   * @param serachString
   */
  @Query(returns => [Library], { nullable: true })
  getLibraryBySearchString(@Arg("searchString") serachString: string): LibraryData[] | undefined {
    const response = this.libraryService.getLibrarys();
    return response.filter(library => {
      return (library.album.toLowerCase().includes(serachString.toLowerCase()) || library.title.toLowerCase().includes(serachString.toLowerCase()) || library.artist.toLowerCase().includes(serachString.toLowerCase()));
    });
  }

  /**
   * get library using duration based on less than and greater than keys
   * @param time
   */
  @Query(returns => [Library], { nullable: true })
  getLibraryByTime(@Arg("time") time: number, @Arg("key") field: string ): LibraryData[] | undefined {
    if(field !=="durationless" && field !=="durationgreater"){
      throw new SyntaxError("key is not valid");
    }
    const response = this.libraryService.getLibrarys();
    return response.filter(library => {
      if(field==="durationless")
        return (library.duration <= time);
      if(field==="durationgreater")
        return (library.duration >= time);
    });
  }
}
